package br.unifil.dc.lab2.Uteis;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Aleatorios {

    public static List<Integer> gerarListaAleatoria(int seed) {
        return gerarListaAleatoria(seed, 20);
    }
    public static List<Integer> gerarListaAleatoria(int seed, int qtde) {
        return gerarListaAleatoria(seed, qtde, 1000);
    }
    public static List<Integer> gerarListaAleatoria(int seed, int qtde, int limSup) {
        List<Integer> valsAleatorios = new ArrayList<>(qtde);
        Random rng = new Random(seed);
        while (qtde-- > 0) {
            valsAleatorios.add(rng.nextInt(limSup));
        }
        return valsAleatorios;
    }
}
