package br.unifil.dc.lab2.TDAs;

public interface Pilha<T> extends Colecao<T> {
    /**
     *
     * @param obj
     */
    void empilhar(T obj);

    /**
     *
     * @return
     */
    T desempilhar();

    /**
     *
     * @return
     */
    T olharTopo();
}
