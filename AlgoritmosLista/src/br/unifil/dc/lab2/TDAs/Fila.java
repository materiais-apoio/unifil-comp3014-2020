package br.unifil.dc.lab2.TDAs;

public interface Fila<T> extends Colecao<T> {
    void enfileirar(T obj);
    T desenfileirar();
    T olharPrimeiro();
}
