package br.unifil.dc.lab2.Testes;

import br.unifil.dc.lab2.Algoritmos.Pesquisadores;

import java.util.Arrays;
import java.util.List;

public class PesquisadoresTests {

    private static List<Integer> listaTesteMutavel = Arrays.asList(4, 2, 3, 4, 5, 4);
    private static List<Integer> listaOrdenadaMutavel = Arrays.asList(1,3,4,5,7,9,10,12,14,15,16);

    private static List<Integer> listaTesteInteger = List.of(4,2,3,4,5,4);
    private static List<Integer> listaOrdenadaInteger = List.of(1,3,4,5,7,9,10,12,14,15,16);

    private static List<String> listaTesteString = List.of("Um", "Dois", "Tres", "Quatro", "Cinco", "Seis");

    public static void testarPesquisasInteger() {
        System.out.println("Esperava 0, tive " + Pesquisadores.pesquisar(listaTesteInteger, 4));
        System.out.println("Esperava empty, tive " + Pesquisadores.pesquisar(listaTesteInteger, 10));
    }

    public static void testarPesquisaSentinela() {
        System.out.println("Esperava 0, tive " + Pesquisadores.pesquisarSentinela(listaTesteMutavel, 4));
        System.out.println("Esperava empty, tive " + Pesquisadores.pesquisarSentinela(listaOrdenadaMutavel, 10));
    }

    public static void testarPesquisasString() {
        System.out.println("Esperava 4, tive " + Pesquisadores.pesquisar(listaTesteString, "Cinco"));
        System.out.println("Esperava empty, tive " + Pesquisadores.pesquisar(listaTesteString, "Zero"));
    }

    public static void testarPesquisasBinarias() {
        System.out.println("Espero 2 mas obtive " + Pesquisadores.pesquisarBinario(listaOrdenadaInteger, 4));
        System.out.println("Espero 5 mas obtive " + Pesquisadores.pesquisarBinario(listaOrdenadaInteger, 9));
        System.out.println("Espero "+ (listaOrdenadaInteger.size()-1) +" mas obtive " + Pesquisadores.pesquisarBinario(listaOrdenadaInteger, 16));
        System.out.println("Espero empty mas obtive " + Pesquisadores.pesquisarBinario(listaOrdenadaInteger, 20));
        System.out.println("Espero empty mas obtive " + Pesquisadores.pesquisarBinario(listaOrdenadaInteger, -3));
    }
}
