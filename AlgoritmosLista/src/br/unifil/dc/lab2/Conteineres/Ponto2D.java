package br.unifil.dc.lab2.Conteineres;

public class Ponto2D<T, U> extends Par<T, U> {
    public Ponto2D(T x, U y) {
        super(x,y);
    }

    public T getX() {
        return getEsquerda();
    }

    public U getY() {
        return getDireita();
    }

    @Override
    public String toString() {
        return "Ponto2D("+getEsquerda()+","+getDireita()+")";
    }
}
