package br.unifil.dc.lab2.Conteineres;

public class Resultado<Erro, Valor> extends Par<Opcional<Erro>, Opcional<Valor>> {

    @SuppressWarnings("unchecked")
    public static <E> Resultado erro(E e) {
        return new Resultado(Opcional.de(e), Opcional.vazio());
    }

    @SuppressWarnings("unchecked")
    public static <V> Resultado de(V v) {
        return new Resultado(Opcional.vazio(), Opcional.de(v));
    }

    public boolean isResultado() {
        return getEsquerda().isVazio();
    }

    public boolean isErro() {
        return !isResultado();
    }

    public Erro getErro() {
        return getEsquerda().get();
    }

    public Valor get() {
        return getDireita().get();
    }

    private Resultado(Opcional<Erro> e, Opcional<Valor> v) {
        super(e,v);
    }
}
